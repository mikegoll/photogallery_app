package com.example.mike_.photogallery;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.example.mike_.utilities.PhotoLocationListener;

import com.example.mike_.dbhelper.DBHelper;
import com.example.mike_.utilities.Utilities;

/** MainActivity
 *
 * Description: The main activity of the application.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     onCreate(Bundle)
 *              onResume()
 *              onActivityResult(int, int, Intent)
 *              onCreateOptionsMenu(Menu)
 *              onNavigationItemSelected(final MenuItem)
 *              onOptionsItemSelected(MenuItem)
 *              refreshGallery()
 *              filterGallery(String, boolean, long, long, double, double, double, double)
 *              buildArrayAdapter(Cursor)
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private ActionBarDrawerToggle toggle;
    private DrawerLayout sideBar;
    private Uri pictureTaken;
    private GridViewAdapter gridViewAdapter;
    private File f;
    private boolean searchViewState = false;
    private SearchView searchView;
    private Context context;
    private LocationListener locListener;
    private LocationManager locMan;

    /** onCreate
     *
     * Description: Creates the activity.
     *
     * Parameters:  Bundle - The bundle of the saved application instance state.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       Sets up the activity.
     *              Sets up the button onclick listeners and populates the gallery.
     *              Scans for a location update and updates the location when it's changed.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();

        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);

        sideBar = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, sideBar, R.string.nav_bar_open, R.string.nav_bar_closed);
        sideBar.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        searchView = (SearchView) findViewById(R.id.searchView);


        //makes the entire search view bar clickable
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
                InputMethodManager im = ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE));
                im.showSoftInput(v, 0);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null)
                    gridViewAdapter.setData(buildArrayAdapter(
                            DBHelper.getInstance(context).filterCaptions(query)));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridViewAdapter = new GridViewAdapter(this, R.layout.gallery_photo_layout, refreshGallery());
        gridView.setAdapter(gridViewAdapter);

        Button snapBtn = (Button) findViewById(R.id.snapBtn);
        snapBtn.setBackgroundColor(ContextCompat.getColor(context, R.color.snapBtnColor));
        snapBtn.setTextColor(Color.WHITE);

        snapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE);
                int index = prefs.getInt("photoIndex", 0);

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                f = new File(Environment.getExternalStorageDirectory(), "IMAGE" + index + ".jpg");

                SharedPreferences.Editor editor = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE).edit();
                editor.putInt("photoIndex", ++index);
                editor.apply();

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                pictureTaken = Uri.fromFile(f);

                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goToPreviewScreen = new Intent(context, ImagePreviewActivity.class);
                goToPreviewScreen.putExtra("uri", gridViewAdapter.getAdapterSet().get(position).getURI().toString());
                goToPreviewScreen.putExtra("caption", gridViewAdapter.getAdapterSet().get(position).getTitle());
                startActivityForResult(goToPreviewScreen, 1);
            }
        });

        locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locListener = new PhotoLocationListener();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            if (!locMan.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Toast.makeText(context, "Without location enabled, this app cannot use some features." +
                        " Please turn on Location.", Toast.LENGTH_LONG).show();
            } else {
                Criteria crit = new Criteria();
                String provider = locMan.getBestProvider(crit, true);
                locMan.getLastKnownLocation(provider);

                if (Utilities.checkForGPS(getPackageManager())) {
                    locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locListener);
                } else {
                    locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, locListener);
                }
            }
        }
    }

    /** onResume
     *
     * Description: Calls the parent's onResume method.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /** onActivityResult
     *
     * Description: Refreshes the gallery to respond to actions taken in other activities.
     *
     * Parameters:  int requestCode - The request code that was used.
     *              int resultCode  - The result code of what returned.
     *              Intent data     - The data that was returned.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Uri uri = pictureTaken;
            getContentResolver().notifyChange(uri, null);
            Bitmap reducedSize = Utilities.getBitmap(uri.getPath(), getContentResolver());

            if (reducedSize != null) {
                Intent goToPreviewScreen = new Intent(this, ImagePreviewActivity.class);
                goToPreviewScreen.putExtra("uri", uri.toString());

                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    //save the photo in the database without location
                    DBHelper.getInstance(this).addPhoto(uri.toString());
                } else {
                    Location curLoc = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    BigDecimal longitude = BigDecimal.valueOf(curLoc.getLongitude());
                    BigDecimal latitude = BigDecimal.valueOf(curLoc.getLatitude());
                    DBHelper.getInstance(this).addPhoto(uri.toString(), longitude, latitude);
                }

                Utilities.addImageToGallery(uri.getPath(), this);
                startActivityForResult(goToPreviewScreen, 0);
            }
        } else if (resultCode == Utilities.FINISH_RESULT_FILTERED) {
            gridViewAdapter.setData(filterGallery(data.getStringExtra("caption"),
                    data.getBooleanExtra("placesExist", false), data.getLongExtra("minTime", 0),
                    data.getLongExtra("maxTime", 0), data.getDoubleExtra("minLatitude", -1),
                    data.getDoubleExtra("minLongitude", -1), data.getDoubleExtra("maxLatitude", -1),
                    data.getDoubleExtra("maxLongitude", -1)));
        } else if (resultCode == Utilities.FINISH_RESULT_FILTERED_RESET || resultCode == Utilities.FINISH_RESULT_CAPTION_ADDED){
            gridViewAdapter.setData(refreshGallery());
        } else {
            gridViewAdapter.setData(refreshGallery());
        }
    }

    /** onCreateOptionsMenu
     *
     * Description: Creates the menu.
     *
     * Parameters:  Menu - The menu to create.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the creation failed.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mI = getMenuInflater();
        mI.inflate(R.menu.main_menu, menu);
        return true;
    }

    /** onNavigationItemSelected
     *
     * Description: Responds to the selection of a menu item.
     *
     * Parameters:  MenuItem - The menu item that was selected.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the selection failed.
     */
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        switch (item.getItemId()) {
            case R.id.ch_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, 1);
                break;

            default:
                Toast.makeText(this, "Not Implemented", Toast.LENGTH_SHORT).show();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /** onOptionsSelected
     *
     * Description: Responds to the selection of a menu item.
     *
     * Parameters:  MenuItem - The menu item that was selected.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the selection failed.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            sideBar.openDrawer(Gravity.START);
        } else if (id == R.id.searchIcon) {
            if (!searchViewState) {
                searchView.setVisibility(View.VISIBLE);
                searchView.setIconified(true);
                searchViewState = true;
            } else {
                searchView.setVisibility(View.GONE);
                searchViewState = false;
            }
        } else if (id == R.id.refreshGallery) {
            gridViewAdapter.setData(refreshGallery());
        }

        return super.onOptionsItemSelected(item);
    }

    /** refreshGallery
     *
     * Description: Refreshes the gallery.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     ArrayList<ImageItem> - The ArrayList containing all ImageItem objects.
     *
     * Notes:       Populates the gallery with all photos in the database.
     */
    private ArrayList<ImageItem> refreshGallery() {
        Cursor c = DBHelper.getInstance(this).getAllUris();

        Log.d("Refresh()", DatabaseUtils.dumpCursorToString(c));

        if (c == null) {
            return new ArrayList<>();
        }

        return buildArrayAdapter(c);
    }

    /** filterGallery
     *
     * Description: Filters the gallery. Responds differently depending on values of the parameters.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     ArrayList<ImageItem> - The ArrayList containing the relevant ImageItem objects.
     */
    private ArrayList<ImageItem> filterGallery(String caption, boolean placesExist, long minTime,
                                               long maxTime, double minLatitude, double minLongitude,
                                               double maxLatitude, double maxLongitude) {
        Cursor c = null;

        //Filtering by caption only
        if (!caption.equals("") && (minTime == 0 && maxTime == 0) && (!placesExist)) {
            c = DBHelper.getInstance(this).filterCaptions(caption);

        //Filtering by Date only
        } else if (caption.equals("") && (minTime > 0 && maxTime > 0) && (!placesExist)) {
            c = DBHelper.getInstance(this).getAllByDateRange(minTime, maxTime);

        //Filtering by Location only
        } else if (caption.equals("") && (minTime == 0 && maxTime == 0) && (placesExist)) {
            c = DBHelper.getInstance(this).filterLocations(minLatitude, minLongitude, maxLatitude, maxLongitude);

        //Filtering by Caption and Date
        } else if (!caption.equals("") && (minTime >= 0 && maxTime >= 0) && (!placesExist)) {
            c = DBHelper.getInstance(this).filterCaptionAndDate(caption, minTime, maxTime);

        //Filtering by Caption and Location
        } else if (!caption.equals("") && (minTime == 0 && maxTime == 0) && (placesExist)) {
            c = DBHelper.getInstance(this).filterCaptionAndLocation(caption, minLatitude, minLongitude,
                    maxLatitude, maxLongitude);

        //Filtering by Date and Location
        } else if (caption.equals("") && (minTime >= 0 && maxTime >= 0) && (placesExist)) {
            c = DBHelper.getInstance(this).filterDateAndLocation(minTime, maxTime, minLatitude,
                    minLongitude, maxLatitude, maxLongitude);

        //Filtering by Caption, Location and Date
        } else {
            c = DBHelper.getInstance(this).filterEverything(caption, minTime, maxTime, minLatitude,
                    minLongitude, maxLatitude, maxLongitude);
        }

        if (c == null) {
            return new ArrayList<>();
        }

        return buildArrayAdapter(c);
    }

    /** buildArrayAdapter
     *
     * Description: Builds the ArrayAdapter based on Cursor entries.
     *
     * Parameters:  Cursor - A cursor containing the data to be built into the ArrayAdapter
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     ArrayList<ImageItem> - The build ArrayList of ImageItem objects.
     */
    private ArrayList<ImageItem> buildArrayAdapter(Cursor c) {

        if (c != null) {
            final int captionCol = c.getColumnIndex(DBHelper.PHOTO_CAPTION);
            final int uriCol = c.getColumnIndex(DBHelper.PHOTO_URI_NAME);

            ArrayList<ImageItem> imageItems = new ArrayList<>();

            //temporary image view to test if the image is valid
            ImageView imgv = new ImageView(MainActivity.this);

            do {
                Uri image = Uri.parse(c.getString(uriCol));

                //If the file is missing or doesn't exist
                try {
                    imgv.setImageURI(image);
                } catch (Exception e) {
                    DBHelper.getInstance(this).removeUri(image.toString());

                    SharedPreferences prefs = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE);
                    int index = prefs.getInt("photoIndex", 0);

                    SharedPreferences.Editor editor = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE).edit();
                    editor.putInt("photoIndex", --index);
                    editor.apply();

                    continue;
                }

                //If the file's drawable is invalid
                if (imgv.getDrawable() == null) {
                    DBHelper.getInstance(this).removeUri(image.toString());

                    SharedPreferences prefs = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE);
                    int index = prefs.getInt("photoIndex", 0);

                    SharedPreferences.Editor editor = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE).edit();
                    editor.putInt("photoIndex", --index);
                    editor.apply();

                    continue;
                }

                Cursor ca = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[] { MediaStore.MediaColumns._ID },
                        MediaStore.MediaColumns.DATA + "=?",
                        new String[] {image.getPath()},
                        null);

                if (ca != null && ca.moveToFirst()) {
                    int id = ca.getInt(ca.getColumnIndex(MediaStore.MediaColumns._ID));
                    ca.close();

                    Bitmap thumbnail = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), id, MediaStore.Images.Thumbnails.MINI_KIND, null);
                    imageItems.add(new ImageItem(thumbnail, c.getString(captionCol), image));
                }
            } while (c.moveToNext());

            c.close();
            DBHelper.getInstance(this).closeDB();

            return imageItems;
        }

        return null;
    }
}
