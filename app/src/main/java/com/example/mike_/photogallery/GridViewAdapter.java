package com.example.mike_.photogallery;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/** GridViewAdapter
 *
 * Description: The gridviewadapter that holds the data for the gallery.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     GridViewAdapter(Context, int, ArrayList<ImageItem>)
 *              getView(int, View, ViewGroup)
 *              setData(ArrayList<ImageItem>)
 *              getAdapterSet()
 *
 * Inner Class: ViewHolder
 */

public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList<ImageItem> data = new ArrayList<>();

    /** getInstance
     *
     * Description: Returns the instance of the DBHelper if it exists, otherwise it calls the constructor.
     *
     * Parameters:  Context context      - The application context needed for the GridViewAdapter.
     *              int layoutResourceId - The layoutResourceId used internally by the GridViewAdapter parent.
     *              ArrayList data       - The ArrayList of data used internally of ImageItem objects.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Notes:       Creates the GridViewAdapter and sets its internal data.
     */
    public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    /** getView
     *
     * Description: Returns the instance of the DBHelper if it exists, otherwise it calls the constructor.
     *
     * Parameters:  int position     - The position in the ArrayList specified for retrieval.
     *              View convertView - The view to be converted.
     *              ViewGroup parent - The parent ViewGroup.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     View - The view that populates the GridView.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = (TextView) row.findViewById(R.id.text);
            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = data.get(position);
        holder.imageTitle.setText(item.getTitle());
        holder.image.setImageBitmap(item.getImage());

        return row;
    }

    /** setData
     *
     * Description: Sets the internal ArrayList that is used to populate the gallery.
     *
     * Parameters:  ArrayList<ImageItem> newDataSet - The new data set to be used.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       The ArrayList "data set" that is passed to this method populates the gallery verbatim.
     */
    public void setData(ArrayList<ImageItem> newDataSet) {
        if (newDataSet != null) {
            this.data.clear();
            this.data.addAll(newDataSet);
            this.notifyDataSetChanged();
        } else {
            this.data.clear();
            this.notifyDataSetChanged();
        }
    }

    /** getAdapterSet
     *
     * Description: Returns the currently used data set.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     ArrayList<ImageItem> - The currently used data set.
     */
    public ArrayList<ImageItem> getAdapterSet() {
        return data;
    }

    /** ViewHolder class
     *
     * Description: The class that defines what the ImageItem is. It holds references for a TextView
     *                   and an ImageView.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     */
    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}
