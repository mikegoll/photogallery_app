package com.example.mike_.photogallery;

import android.graphics.Bitmap;
import android.net.Uri;

/** ImageItem
 *
 * Description: Class that defines what each item in the GridView is.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     ImageItem(Bitmap, String, Uri)
 *              getImage()
 *              getTitle()
 *              getURI()
 */

public class ImageItem {
    private Bitmap image;
    private String title;
    private Uri uri;

    /** ImageItem
     *
     * Description: ImageItem constructor.
     *
     * Parameters:  Bitmap image - The bitmap stored in the object.
     *              String title - The title/caption of the image.
     *              Uri uri      - The uri of the image.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     */
    public ImageItem(Bitmap image, String title, Uri uri) {
        super();
        this.image = image;
        this.title = title;
        this.uri = uri;
    }

    /** getImage
     *
     * Description: Gets the image stored in the object.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Bitmap - The bitmap of the object.
     */
    public Bitmap getImage() {
        return image;
    }

    /** getTitle
     *
     * Description: Returns the title/caption associated with the image.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     String - The title/caption of the image.
     */
    public String getTitle() {
        return title;
    }

    /** getURI
     *
     * Description: Returns the uri of the image.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Uri - The uri of the image.
     */
    public Uri getURI() { return uri; }
}