package com.example.mike_.photogallery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.String;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mike_.utilities.Utilities;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

/** SettingsActivity
 *
 * Description: Settings activity that handles the UI for the filtering of photos.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     onCreate(Bundle)
 *              onActivityResult(int, int, Intent)
 *              onOptionsItemSelected(MenuItem)
 */
public class SettingsActivity extends AppCompatActivity {

    private Button minDateText, maxDateText, minLocationText, maxLocationText;
    private Calendar minCal = Calendar.getInstance();
    private Calendar maxCal = Calendar.getInstance();
    private Place minPlace, maxPlace;
    private EditText caption;

    /** onCreate
     *
     * Description: Creates the activity.
     *
     * Parameters:  Bundle - The bundle of the saved application instance state.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       Sets up the activity.
     *              Sets up the button onclick listeners and readies the dialogs.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        caption = (EditText) findViewById(R.id.captionFilterText);

        minDateText = (Button) findViewById(R.id.startDateText);
        maxDateText = (Button) findViewById(R.id.endDateText);

        minLocationText = (Button) findViewById(R.id.startLocationText);
        maxLocationText = (Button) findViewById(R.id.endLocationText);

        final DatePickerDialog.OnDateSetListener maxDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int maxDay = view.getDayOfMonth();
                int maxMonth = view.getMonth();
                int maxYear = view.getYear();

                maxCal.set(maxYear, maxMonth, maxDay);
                maxCal.set(Calendar.HOUR_OF_DAY, 0);
                maxCal.set(Calendar.MINUTE, 0);
                maxCal.set(Calendar.SECOND, 0);
                maxCal.set(Calendar.MILLISECOND, 0);

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.CANADA);
                String formattedDate = sdf.format(maxCal.getTime());

                maxDateText.setText(formattedDate);
            }
        };

        final DatePickerDialog.OnDateSetListener minDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int minDay = view.getDayOfMonth();
                int minMonth = view.getMonth();
                int minYear = view.getYear();

                minCal.set(minYear, minMonth, minDay);
                minCal.set(Calendar.HOUR_OF_DAY, 0);
                minCal.set(Calendar.MINUTE, 0);
                minCal.set(Calendar.SECOND, 0);
                minCal.set(Calendar.MILLISECOND, 0);

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.CANADA);
                String formattedDate = sdf.format(minCal.getTime());

                minDateText.setText(formattedDate);
            }
        };

        minDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SettingsActivity.this, minDate, minCal.get(Calendar.YEAR), minCal.get(Calendar.MONTH), minCal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        maxDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SettingsActivity.this, maxDate, maxCal.get(Calendar.YEAR), maxCal.get(Calendar.MONTH), maxCal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        minLocationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(SettingsActivity.this), Utilities.MIN_PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });

        maxLocationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(SettingsActivity.this), Utilities.MAX_PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });


        Button filterBtn = (Button) findViewById(R.id.filterBtn);
        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();

                i.putExtra("caption", caption.getText().toString());

                if (minDateText.getText().toString().toLowerCase().equals("start date"))
                    i.putExtra("minTime", 0);
                else
                    i.putExtra("minTime", minCal.getTimeInMillis());

                if (maxDateText.getText().toString().toLowerCase().equals("end date"))
                    i.putExtra("maxTime", 0);
                else
                    i.putExtra("maxTime", maxCal.getTimeInMillis());

                //for the espresso tests
                //espresso cannot reach the datepicker dialog
                //so I just enter the values manually
//                i.putExtra("minTime", 1508050800000L);
//                i.putExtra("maxTime", 1508223540000L);

                if (minPlace != null && maxPlace != null) {
                    i.putExtra("placesExist", true);

                    i.putExtra("minLatitude", (Double) minPlace.getLatLng().latitude);
                    i.putExtra("minLongitude", (Double) minPlace.getLatLng().longitude);
                    i.putExtra("maxLatitude", (Double) maxPlace.getLatLng().latitude);
                    i.putExtra("maxLongitude", (Double) maxPlace.getLatLng().longitude);
                } else {
                    i.putExtra("placesExist", false);

                    i.putExtra("minLatitude", -1);
                    i.putExtra("minLongitude", -1);
                    i.putExtra("maxLatitude", -1);
                    i.putExtra("maxLongitude", -1);
                }

                if (caption.getText().toString().equals("") && minDateText.getText().toString().toLowerCase().equals("start date")
                        && maxDateText.getText().toString().toLowerCase().equals("end date")
                        && minPlace == null && maxPlace == null) {
                    Toast.makeText(getApplicationContext(), "Invalid Filter Settings", Toast.LENGTH_LONG).show();
                    return;
                }

                setResult(Utilities.FINISH_RESULT_FILTERED, i);
                finish();
            }
        });

        Button resetFilterBtn = (Button) findViewById(R.id.resetFilterBtn);
        resetFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Utilities.FINISH_RESULT_FILTERED_RESET);
                finish();
            }
        });
    }

    /** onActivityResult
     *
     * Description: Refreshes the gallery to respond to actions taken in other activities.
     *
     * Parameters:  int requestCode - The request code that was used.
     *              int resultCode  - The result code of what returned.
     *              Intent data     - The data that was returned.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Utilities.MIN_PLACE_PICKER_REQUEST:
                    minPlace = PlacePicker.getPlace(getApplicationContext(), data);
                    String locationText = "(" + Double.toString(minPlace.getLatLng().latitude) + "), " +
                            "(" + Double.toString(minPlace.getLatLng().longitude) + ")";
                    minLocationText.setText(locationText);
                    break;
                case Utilities.MAX_PLACE_PICKER_REQUEST:
                    maxPlace = PlacePicker.getPlace(getApplicationContext(), data);
                    String mLocationText = "(" + Double.toString(maxPlace.getLatLng().latitude) + "), " +
                            "(" + Double.toString(maxPlace.getLatLng().longitude) + ")";
                    maxLocationText.setText(mLocationText);
                    break;
            }
        }
    }

    /** onOptionsSelected
     *
     * Description: Responds to the selection of a menu item.
     *
     * Parameters:  MenuItem - The menu item that was selected.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the selection failed.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
