package com.example.mike_.photogallery;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mike_.dbhelper.DBHelper;
import com.example.mike_.utilities.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/** ImagePreviewActivity
 *
 * Description: Activity that is used to display the image along with its caption (if it has one).
 *              Also displays 3 buttons - delete, change caption and share.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     onCreate(Bundle)
 */
public class ImagePreviewActivity extends AppCompatActivity {

    /** onCreate
     *
     * Description: Creates the activity.
     *
     * Parameters:  Bundle - The bundle of the saved application instance state.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       Sets up the activity.
     *              Sets up the button onclick listeners and populates the ImageView with the previously
     *                  specified photo.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        final ImageView imgView = (ImageView) findViewById(R.id.imagePreview);
        final Button deleteBtn = (Button) findViewById(R.id.deleteBtn);
        final TextView captionView = (TextView) findViewById(R.id.captionView);
        final Button captionBtn = (Button) findViewById(R.id.captionBtn);
        final Button shareBtn = (Button) findViewById(R.id.shareBtn);

        final Intent i = getIntent();
        final String uri = i.getStringExtra("uri");
        final String caption = i.getStringExtra("caption");
        final Uri image = Uri.parse(uri);

        final Bitmap photo = BitmapFactory.decodeFile(image.getPath());

        //For whatever reason, this scaling ensures that the image appears in the image view.
        Bitmap resizedPhoto = Bitmap.createScaledBitmap(photo, (int) (photo.getWidth() * 0.5), (int) (photo.getHeight() * 0.5), false);

        imgView.setImageBitmap(resizedPhoto);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //pop up the dialog to ask if they want to delete the photo
                final Dialog d = new Dialog(ImagePreviewActivity.this);
                d.setContentView(R.layout.dialog_delete_photo);
                d.setTitle("Delete Photo?");

                TextView t = (TextView) d.findViewById(R.id.textView2);
                t.setText(R.string.delete_confirm);

                Button yesBtn = (Button) d.findViewById(R.id.button);
                yesBtn.setText(R.string.confirm_action);
                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String filePath = Utilities.getFilePath(image, getContentResolver());

                        if (Utilities.deletePhoto(filePath)) {
                            DBHelper.getInstance(getApplicationContext()).removeUri(uri);

                            SharedPreferences prefs = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE);
                            int index = prefs.getInt("photoIndex", 0);

                            SharedPreferences.Editor editor = getSharedPreferences(Utilities.MY_PREFS, MODE_PRIVATE).edit();
                            editor.putInt("photoIndex", --index);
                            editor.apply();

                            d.dismiss();
                            finish();
                        } else {
                            d.dismiss();
                            Toast.makeText(ImagePreviewActivity.this, "Could not delete photo.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Button noBtn = (Button) d.findViewById(R.id.button2);
                noBtn.setText(R.string.cancel_action);
                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });

                d.show();
            }
        });

        captionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //pop up the dialog to ask if they want to delete the photo
                final Dialog d = new Dialog(ImagePreviewActivity.this);
                d.setContentView(R.layout.dialog_add_caption);
                d.setTitle("Add Caption");

                final TextView t = (TextView) d.findViewById(R.id.editText);

                Button yesBtn = (Button) d.findViewById(R.id.addBtn);
                yesBtn.setText(R.string.add);
                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String caption = t.getText().toString();
                        if (!(caption.isEmpty())) {
                            DBHelper.getInstance(getApplicationContext()).updateCaption(uri, caption);
                            captionView.setText(caption);
                            captionView.setVisibility(View.VISIBLE);
                            setResult(Utilities.FINISH_RESULT_CAPTION_ADDED);
                        }
                        d.dismiss();
                    }
                });

                Button noBtn = (Button) d.findViewById(R.id.cancelBtn);
                noBtn.setText(R.string.cancel);
                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });

                d.show();
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                try {
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));
                startActivity(Intent.createChooser(share, "Share Image"));
            }
        });

        //hide the caption bar if there is no caption
        if (caption != null) {
            if (!caption.equals("")) {
                captionView.setText(caption);
                captionView.setVisibility(View.VISIBLE);
            }
        } else {
            captionView.setVisibility(View.INVISIBLE);
        }
    }
}
