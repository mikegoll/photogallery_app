package com.example.mike_.utilities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import android.widget.Toast;

import com.example.mike_.photogallery.MainActivity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/** Utilities
 *
 * Description: Used to house most of the Android specific functions such as saving photos to the Gallery.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     getFilePath(Uri, ContentResolver)
 *              deletePhoto(String)
 *              addImageToGallery(final String, final Context)
 *              getBitmap(String, ContentResolver)
 *              checkForGPS(PackageManager)
 */

public class Utilities {

    public static final int FINISH_RESULT_FILTERED = 2, FINISH_RESULT_FILTERED_RESET = 3, FINISH_RESULT_CAPTION_ADDED = 4,
            MIN_PLACE_PICKER_REQUEST = 99, MAX_PLACE_PICKER_REQUEST = 100;
    public static final String MY_PREFS = "sharedPrefs";

    /** getFilePath
     *
     * Description: Gets the file path from a URI.
     *
     * Parameters:  Uri - The URI path for the file.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     String - The file path of the specified photo.
     */
    public static String getFilePath(Uri uri, ContentResolver cr) {
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor ca = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.MediaColumns.DATA },
                MediaStore.MediaColumns.DATA + "=?",
                new String[] {uri.getPath()},
                null);

        if (ca != null && ca.moveToFirst()) {
            int columnIndex = ca.getColumnIndex(projection[0]);
            String picturePath = ca.getString(columnIndex);
            ca.close();
            return picturePath;
        } else {
            return null;
        }
    }

    /** deletePhoto
     *
     * Description: Deletes a photo from the gallery.
     *
     * Parameters:  String - Path of the file to be deleted.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the photo was deleted successfully.
     */
    public static boolean deletePhoto(String filePath) {
        File delete = new File(filePath);

        if (delete.exists()) {
            return delete.delete();
        }
        return false;
    }

    /** addImageToGallery
     *
     * Description: Adds an image to the gallery.
     *
     * Parameters:  String - Path of the file to be deleted.
     *              Context - The current application context.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public static void addImageToGallery(final String filePath, final Context context) {
        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PermissionChecker.PERMISSION_GRANTED) {
            context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } else {
            Toast.makeText(context, "Oops, we don't have that permission.", Toast.LENGTH_LONG).show();
        }
    }

    /** getBitmap
     *
     * Description: Gets the bitmap of a file.
     *
     * Parameters:  String - Path of the file to be deleted.
     *              ContentResolver - The ContentResolver object to access the Android file system.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Bitmap - The bitmap of the image.
     */
    public static Bitmap getBitmap(String path, ContentResolver cr) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = cr.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap b;
            in = cr.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }

            try {
                in.close();
            } catch (NullPointerException e) {
                Log.d("Utilities:getBitmap", e.getMessage());
            }

            return b;

        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    /** checkForGPS
     *
     * Description: Checks for whether or not the device is GPS enabled.
     *
     * Parameters:  PackageManager - PackageManager used to access the Android system.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     boolean - Whether or not the device is GPS enabled.
     */
    public static boolean checkForGPS(PackageManager packageManager) {
        return  packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }
}
