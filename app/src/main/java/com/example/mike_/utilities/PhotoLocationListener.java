package com.example.mike_.utilities;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

/** PhotoLocationListener
 *
 * Description: Listens for a location change and updates the location.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     onLocationChanged(Location)
 *              onProviderDisabled(String)
 *              onProviderEnabled(String)
 *              onStatusChanged(String, int, Bundle)
 */

public class PhotoLocationListener implements LocationListener {

    private Location location;

    @Override
    public void onLocationChanged(Location loc) {
        Log.d("Location Changed:", "Loc: " + loc.getLatitude() + " Long: " + loc.getLongitude());
        location = loc;
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
