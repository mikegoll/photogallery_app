package com.example.mike_.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.math.BigDecimal;
import java.util.Calendar;

/** DBHelper
 *
 * Description: The interface between the application and the database.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     DBHelper(Context)
 *              getInstance(Context)
 *              onCreate(SQLiteDatabase)
 *              onUpgrade(SQLiteDatabase)
 *              closeDB()
 *              addPhoto(String)
 *              addPhoto(String, double, double)
 *              addPhoto(String, long, double, double)
 *              updateCaption(String, String)
 *              getAllUris()
 *              getAllByDateRange(long, long)
 *              removeUri(String)
 *              removeUri(long)
 *              filterCaptions(String)
 *              filterLocations(double, double, double, double)
 *              filterCaptionAndDate(String, long, long)
 *              filterCaptionAndLocation(String, double, double, double, double)
 *              filterDateAndLocation(long, long, double, double, double, double)
 *              filterEverything(String, long, long, double, double, double, double)
 *
 * Notes:       Singleton that needs an application context from an activity. Only one instance can
 *                  be created at any time and cannot be instantiated externally.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper instance = null;

    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Photos.db";

    private static final String PHOTO_TABLE_NAME = "photos";
    public static final String PHOTO_URI_NAME = "uri";
    public static final String PHOTO_CAPTION = "caption";
    private static final String PHOTO_DATE = "date";
    private static final String PHOTO_LOCATION_LAT = "latitude";
    private static final String PHOTO_LOCATION_LONG = "longitude";

    private static final String CREATE_URI_TABLE =
            "CREATE TABLE IF NOT EXISTS " + PHOTO_TABLE_NAME + " (" +
                    "_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    PHOTO_URI_NAME + " VARCHAR(256) NOT NULL, " +
                    PHOTO_CAPTION + " TEXT NULL, " +
                    PHOTO_DATE + " INTEGER NOT NULL, " +
                    PHOTO_LOCATION_LAT + " REAL NULL, " +
                    PHOTO_LOCATION_LONG + " REAL NULL)";

    private static final String DELETE_URI_ENTRIES =
            "DELETE FROM " + PHOTO_TABLE_NAME + " WHERE 1=1";

    /** DBHelper
     *
     * Description: DBHelper constructor.
     *
     * Parameters:  Context context - The application context needed for the SQLiteDatabase.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Notes:       Only called internally when a DBHelper instance is not already
     *                  created.
     */
    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /** getInstance
     *
     * Description: Returns the instance of the DBHelper if it exists, otherwise it calls the constructor.
     *
     * Parameters:  Context context - The application context needed for the SQLiteDatabase.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     DBHelper - The instance of the DBHelper.
     *
     * Notes:       Only called internally when a DBHelper instance is not already
     *                  created.
     */
    public static DBHelper getInstance (Context context) {
        if (instance == null) {
            instance = new DBHelper(context.getApplicationContext());
        }
        return instance;
    }

    /** onCreate
     *
     * Description: Creates the database table when it is initially created.
     *
     * Parameters:  SQLiteDatabase - The SQLite database reference.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_URI_TABLE);
    }

    /** onUpgrade
     *
     * Description: Deletes and recreates the database when the version is changed.
     *
     * Parameters:  Context context - The application context needed for the SQLiteDatabase.
     *              int oldVersion  - The old version of the database.
     *              int newVersion  - The new version of the database.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_URI_ENTRIES);
        onCreate(db);
    }

    /** closeDB
     *
     * Description: Closes the database if it is open.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }


    /** addPhoto
     *
     * Description: Adds a photo to the database.
     *
     * Parameters:  String uri - The uri of the photo to be added to the database.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void addPhoto(String uri) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(PHOTO_URI_NAME, uri);
            cv.put(PHOTO_DATE, Calendar.getInstance().getTimeInMillis());

            db.insertOrThrow(PHOTO_TABLE_NAME, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** addPhoto
     *
     * Description: Adds a photo to the database.
     *
     * Parameters:  String uri - The uri of the photo to be added.
     *              double longitude - The longitude of where the photo was taken.
     *              double latitude - The latitude of where the photo was taken.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void addPhoto(String uri, BigDecimal longitude, BigDecimal latitude) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(PHOTO_URI_NAME, uri);
            cv.put(PHOTO_DATE, Calendar.getInstance().getTimeInMillis());
            cv.put(PHOTO_LOCATION_LAT, latitude.doubleValue());
            cv.put(PHOTO_LOCATION_LONG, longitude.doubleValue());

            db.insertOrThrow(PHOTO_TABLE_NAME, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** addPhoto
     *
     * Description: Adds a photo to the database.
     *
     * Parameters:  String uri - The uri of the photo to be added.
     *              double longitude - The longitude of where the photo was taken.
     *              double latitude - The latitude of where the photo was taken.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       Used to test database caption filtering.
     */
    public void addPhoto(String uri, String caption, long date, BigDecimal longitude, BigDecimal latitude) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(PHOTO_URI_NAME, uri);
            cv.put(PHOTO_CAPTION, caption);
            cv.put(PHOTO_DATE, date);
            cv.put(PHOTO_LOCATION_LAT, latitude.toString());
            cv.put(PHOTO_LOCATION_LONG, longitude.toString());

            db.insertOrThrow(PHOTO_TABLE_NAME, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** addPhoto
     *
     * Description: Adds a photo to the database.
     *
     * Parameters:  String uri       - The uri of the photo to be added.
     *              long date        - The date the photo was taken in milliseconds.
     *              double longitude - The longitude of where the photo was taken.
     *              double latitude  - The latitude of where the photo was taken.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes:       The method is used to add photos with an arbitrary date. Used mostly for database
     *                  testing.
     */
    public void addPhoto(String uri, long date, BigDecimal longitude, BigDecimal latitude) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(PHOTO_URI_NAME, uri);
            cv.put(PHOTO_DATE, date);
            cv.put(PHOTO_LOCATION_LAT, latitude.toString());
            cv.put(PHOTO_LOCATION_LONG, longitude.toString());

            db.insertOrThrow(PHOTO_TABLE_NAME, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** updateCaption
     *
     * Description: Updates (or adds) a caption to a specified photo.
     *
     * Parameters:  String uri     - The uri of the photo which caption is being changed.
     *              String caption - The new caption to be changed to
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    public void updateCaption(String uri, String caption) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_CAPTION, caption);
        db.update(PHOTO_TABLE_NAME, cv, PHOTO_URI_NAME + " = ?", new String[]{uri});
    }

    /** getAllUris
     *
     * Description: Returns all the uris in the database.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor with all of the uris in the database.
     */
    public Cursor getAllUris() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT DISTINCT * FROM " + PHOTO_TABLE_NAME, null);

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** getAllByDateRange
     *
     * Description: Gets all of the pictures with dates in between two date values.
     *
     * Parameters:  long min - The minimum date of the range; the starting date.
     *              long max - The maximum date of the range; the ending date.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains all table entries that satisfy the query criteria.
     */
    public Cursor getAllByDateRange(long min, long max) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE " + PHOTO_DATE +
            " >= " + min + " AND " + PHOTO_DATE + " <= " + max + " ORDER BY " + PHOTO_DATE + " ASC", null);

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** removeUri
     *
     * Description: Deletes the invalid URI from the database.
     *
     * Parameters:  String uri - The uri of the photo which is now invalidated.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes: This method is called when an invalid URI is retrieved by the gallery. Most common cause
     *            for the invalid URI is a photo that was deleted externally.
     */
    public void removeUri(String uri) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PHOTO_TABLE_NAME, PHOTO_URI_NAME + "=?", new String[] {uri});
    }

    /** removeUri
     *
     * Description: Removes the URIs for all photos with a specified date.
     *
     * Parameters:  long date - The date (in milliseconds) of photos to be deleted.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     *
     * Notes: Used for testing the database date filtering.
     */
    public void removeUri(long date) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PHOTO_TABLE_NAME, PHOTO_DATE + "=?", new String[] {Long.toString(date)});
    }


    //********************************* FILTERS *********************************

    /** filterCaptions
     *
     * Description: Filters the captions for one that roughly matches the search query.
     *
     * Parameters:  String searchQuery - The caption that is searched against.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries with a roughly matching caption.
     *
     * Notes: The entries that are returned contain the search query somewhere inside their caption.
     *            The caption does not need to fully match the search query.
     */
    public Cursor filterCaptions(String searchQuery) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE "
                + PHOTO_CAPTION + " LIKE ?", new String[] {"%" + searchQuery + "%"});

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** filterLocations
     *
     * Description: Retrieves photo entries based on a search area.
     *
     * Parameters:  double minLatitude  - The minimum latitude of the search area; the starting latitude.
     *              double minLongitude - The minimum longitude of the search area; the starting longitude.
     *              double maxLatitude  - The maximum latitude of the search area; the ending latitude
     *              double maxLongitude - THe maximum longitude of the search area; the ending longitude.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries that satisfy the search criteria.
     *
     * Notes:       The minimum location creates the "top-left" corner of the area that is searched.
     *              The maximum location creates the "bottom-right" corner of the area that is searched.
     */
    public Cursor filterLocations(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c;

        if (minLatitude > maxLatitude) {
            c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE "
                    + PHOTO_LOCATION_LAT + " <= " + minLatitude + " AND "
                    + PHOTO_LOCATION_LAT + " >= " + maxLatitude + " AND " + PHOTO_LOCATION_LONG + " >= "
                    + minLongitude + " AND " + PHOTO_LOCATION_LONG + " <= " + maxLongitude, new String[] {});
        } else {
            c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE "
                    + PHOTO_LOCATION_LAT + " >= " + minLatitude + " AND "
                    + PHOTO_LOCATION_LAT + " <= " + maxLatitude + " AND " + PHOTO_LOCATION_LONG + " >= "
                    + minLongitude + " AND " + PHOTO_LOCATION_LONG + " <= " + maxLongitude, new String[] {});
        }

        Log.d("locFiltered", DatabaseUtils.dumpCursorToString(c));

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** filterCaptionAndDate
     *
     * Description: Retrieves photo entries based on a caption and a date range.
     *
     * Parameters:  String caption - The caption to be searched against.
     *              long minDate   - The minimum date to be searched against.
     *              long maxDate   - The maximum date to be searched against.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries that satisfy the search criteria.
     */
    public Cursor filterCaptionAndDate(String caption, long minDate, long maxDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE " +
                PHOTO_CAPTION + " LIKE ? AND " + PHOTO_DATE + " >= " + minDate + " AND " + PHOTO_DATE
                + " <= " + maxDate, new String[] {"%" + caption + "%"});

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** filterCaptionAndLocation
     *
     * Description: Retrieves photo entries based on a caption and a search area.
     *
     * Parameters:  String caption      - The caption to be searched against.
     *              double minLatitude  - The minimum latitude of the search area; the starting latitude.
     *              double minLongitude - The minimum longitude of the search area; the starting longitude.
     *              double maxLatitude  - The maximum latitude of the search area; the ending latitude
     *              double maxLongitude - THe maximum longitude of the search area; the ending longitude.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries that satisfy the search criteria.
     */
    public Cursor filterCaptionAndLocation(String caption, double minLatitude,
                                           double minLongitude, double maxLatitude, double maxLongitude) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE " +
                PHOTO_CAPTION + " LIKE ? AND " + PHOTO_LOCATION_LAT + " <= " + minLatitude + " AND "
                + PHOTO_LOCATION_LAT + " >= " + maxLatitude + " AND " + PHOTO_LOCATION_LONG + " >= "
                + minLongitude + " AND " + PHOTO_LOCATION_LONG + " <= " + maxLongitude, new String[] {"%" + caption + "%"});

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** filterDateAndLocation
     *
     * Description: Retrieves photo entries based on a date range and a search area.
     *
     * Parameters:  long minDate        - The minimum date to be searched against.
     *              long maxDate        - The maximum date to be searched against.
     *              double minLatitude  - The minimum latitude of the search area; the starting latitude.
     *              double minLongitude - The minimum longitude of the search area; the starting longitude.
     *              double maxLatitude  - The maximum latitude of the search area; the ending latitude
     *              double maxLongitude - THe maximum longitude of the search area; the ending longitude.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries that satisfy the search criteria.
     */
    public Cursor filterDateAndLocation(long minDate, long maxDate, double minLatitude, double minLongitude,
                                        double maxLatitude, double maxLongitude) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE " +
                PHOTO_LOCATION_LAT + " <= " + minLatitude + " AND "
                + PHOTO_LOCATION_LAT + " >= " + maxLatitude + " AND " + PHOTO_LOCATION_LONG + " >= "
                + minLongitude + " AND " + PHOTO_LOCATION_LONG + " <= " + maxLongitude + " AND "
                + PHOTO_DATE + " >= " + minDate + " AND " + PHOTO_DATE + " <= " + maxDate, new String[] {});

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }

    /** filterEverything
     *
     * Description: Retrieves photo entries based on a caption, date range and a search area.
     *
     * Parameters:  String caption      - The caption to be searched against.
     *              long minDate        - The minimum date to be searched against.
     *              long maxDate        - The maximum date to be searched against.
     *              double minLatitude  - The minimum latitude of the search area; the starting latitude.
     *              double minLongitude - The minimum longitude of the search area; the starting longitude.
     *              double maxLatitude  - The maximum latitude of the search area; the ending latitude
     *              double maxLongitude - THe maximum longitude of the search area; the ending longitude.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     Cursor - A cursor that contains the entries that satisfy the search criteria.
     */
    public Cursor filterEverything(String caption, long minDate, long maxDate, double minLatitude,
                                   double minLongitude, double maxLatitude, double maxLongitude) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + PHOTO_TABLE_NAME + " WHERE " +
                PHOTO_CAPTION + " LIKE ? AND " + PHOTO_LOCATION_LAT + " <= " + minLatitude + " AND "
                + PHOTO_LOCATION_LAT + " >= " + maxLatitude + " AND " + PHOTO_LOCATION_LONG + " >= "
                + minLongitude + " AND " + PHOTO_LOCATION_LONG + " <= " + maxLongitude + " AND "
                + PHOTO_DATE + " >= " + minDate + " AND " + PHOTO_DATE + " <= " + maxDate, new String[] {"%" + caption + "%"});

        if (c != null && c.moveToFirst()) {
            return c;
        }

        return null;
    }
}
