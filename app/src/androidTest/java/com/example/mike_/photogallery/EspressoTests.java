package com.example.mike_.photogallery;

import android.app.DatePickerDialog;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.NavigationViewActions;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Gravity;
import android.widget.DatePicker;
import android.widget.GridView;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;

/**
 * Created by Michael on 2017-10-09.
 */

@RunWith(AndroidJUnit4.class)
public class EspressoTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    /** ensureCaptionFilteringWorks
     *
     * Description: - Ensures that the shorthand method of filtering by captions works.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void ensureCaptionFilteringWorks() {
        String testString = "water";
        onView(withId(R.id.searchIcon)).perform(click());
        onView(withId(R.id.searchView)).perform(typeText(testString + "\n"));
        onView(withText(testString)).check(matches(isDisplayed()));
    }


    /** ensureLongCaptionFilteringWorks
     *
     * Description: - Ensures that the long way of filtering by captions through the filtering screen
     *                    works.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void ensureLongCaptionFilteringWorks() {
        String testString = "water";

        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.START))) // Left Drawer should be closed.
                .perform(open());

        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.ch_settings));

        onView(withId(R.id.captionFilterText)).perform(typeText(testString));

        Espresso.closeSoftKeyboard();
        onView(withId(R.id.filterBtn)).perform(click());

        onView(withText(testString)).check(matches(isDisplayed()));
    }
}