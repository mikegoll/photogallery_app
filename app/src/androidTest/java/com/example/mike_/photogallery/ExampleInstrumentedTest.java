package com.example.mike_.photogallery;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.intent.IntentStubber;

import com.example.mike_.dbhelper.DBHelper;
import com.example.mike_.utilities.Utilities;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.*;

/** ExampleInstrumentedTest
 *
 * Description: Instrument Test used to test internal parts such as the database.
 *
 * Designer:    Michael Goll
 * Programmer:  Michael Goll
 *
 * Methods:     addMockImages()
 *              useAppContext()
 *              databaseAddingIsWorking()
 *              filterByCaptionIsWorking()
 *              filterByTimeStampIsWorking()
 *              filterByLocationIsWorking()
 *              databaseRemoveIsWorking()
 *              cleanUp()
 *
 * Notes:       Ensure that the "photosAlreadyLoaded" variable is correctly set to the corresponding
 *                  number of photos that are already taken with the application prior to testing. If
 *                  no photos are taken and the application data is clean, simply set it to zero.
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private static DBHelper instance;
    private int photosAlreadyLoaded = 0;

    /** addMockImages
     *
     * Description: Inserts mock image data before all tests are executed.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @BeforeClass
    public static void addMockImages() throws Exception {

        instance = DBHelper.getInstance(InstrumentationRegistry.getTargetContext());

        for (int i = 5; i < 15; i++) {
            //October 3, 2017 timestamp
            BigDecimal lat = BigDecimal.valueOf(0.0);
            BigDecimal longi = BigDecimal.valueOf(0.0);
            instance.addPhoto("/storage/emulated/0/" + i + ".jpg", 1507014000100L, lat, longi);
        }

        for (int j = 16; j < 26; j++) {
            //October 10, 2017 timestamp
            BigDecimal lat2 = BigDecimal.valueOf(2.0);
            BigDecimal longi2 = BigDecimal.valueOf(2.0);
            instance.addPhoto("/storage/emulated/0/" + j + ".jpg", "testCaption", 1507532400000L, lat2, longi2);
        }
    }

    /** useAppContext
     *
     * Description: Tests whether or not the application context is used correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.example.mike_.photogallery", appContext.getPackageName());
    }

    /** databaseAddingIsWorking
     *
     * Description: Tests whether or not the database is adding photos correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void databaseAddingIsWorking() throws Exception {
        ArrayList<String> titles = new ArrayList<>();

        Cursor c = instance.getAllUris();

        if (c != null) {
            do {
                titles.add(c.getString(c.getColumnIndex("uri")));
            } while (c.moveToNext());
        }

        assertEquals(20 + photosAlreadyLoaded, titles.size());
    }

    /** filterByCaptionIsWorking
     *
     * Description: Tests whether or not caption filtering works correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void filterByCaptionIsWorking() throws Exception {
        ArrayList<String> titles = new ArrayList<>();

        Cursor c = instance.filterCaptions("testCaption");

        if (c != null) {
            do {
                titles.add(c.getString(c.getColumnIndex("uri")));
            } while (c.moveToNext());
        }

        assertEquals(10 + photosAlreadyLoaded, titles.size());
    }

    /** filterByTimeStampIsWorking
     *
     * Description: Tests whether or not timestamp filtering works correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void filterByTimeStampIsWorking() throws Exception {
        ArrayList<String> titles = new ArrayList<>();

        //tests for images between October 3, 2017 and October 5, 2017 in milliseconds
        Cursor c = instance.getAllByDateRange(1507014000000L, 1507186800000L);

        //The database already moves the cursor to the first entry
        if (c != null) {

            do {
                titles.add(c.getString(c.getColumnIndex("caption")));
            } while (c.moveToNext());
        }

        assertEquals(10 + photosAlreadyLoaded, titles.size());
    }

    /** filterByLocationIsWorking
     *
     * Description: Tests whether or not location filtering works correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void filterByLocationIsWorking() throws Exception {
        ArrayList<String> entries = new ArrayList<>();
        double minLat = 1;
        double minLong = -1;
        double maxLat = -1;
        double maxLong = 1;

        Cursor c = instance.filterLocations(minLat, minLong, maxLat, maxLong);

        if (c != null) {
            do {
                entries.add(c.getString(c.getColumnIndex("caption")));
            } while (c.moveToNext());
        }

        assertEquals(10 + photosAlreadyLoaded, entries.size());
    }


    /** databaseRemoveIsWorking
     *
     * Description: Tests whether or not database removal works correctly.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @Test
    public void databaseRemoveIsWorking() {
        instance.removeUri(1507014000100L);
        instance.removeUri(1507532400000L);

        Cursor c = instance.getAllUris();

        //Cursor returns null because the database cannot
        //move it to the first entry—there are none.

        if (photosAlreadyLoaded > 0)
            assertEquals(photosAlreadyLoaded, c.getCount());
        else
            assertEquals(null, c);

    }

    /** cleanUp
     *
     * Description: -Closes the database to avoid any memory/resource leaks.
     *
     * Designer:    Michael Goll
     * Programmer:  Michael Goll
     *
     * Returns:     void
     */
    @AfterClass
    public static void cleanUp() throws Exception {
        instance.closeDB();
    }
}
